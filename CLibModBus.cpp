#include "CLibModBus.h"
CLibModBus::CLibModBus()
	: m_modBus(nullptr)
{
}

CLibModBus::~CLibModBus()
{
	DisConnectDevice();
}

void CLibModBus::HandleError(const std::string& text)
{
	m_errorString = text + ":" + std::string(modbus_strerror(errno));
	//emit SignalErrorOccurred(m_errorString);
}

bool CLibModBus::ConnectDevice(const std::string& portName, int slave)
{
	//如果已经连接需要先断开连接
	if (m_modBus != nullptr)
		DisConnectDevice();

	//以Rtu的方式创建一个modbus_t实例
	//parity: 可选的有 N:无校验,E:偶校验,O(英文字母):奇校验
	//data_bit:数据位
	//stop_bit:停止位
	m_modBus = modbus_new_rtu(portName.c_str(), 19200, 'E', 8, 1);

	//设置主站地址
	if (modbus_set_slave(m_modBus, 1) != 0)
	{
		//异常处理
		HandleError("初始化Modbus客户端失败");
		return false;
	}

	//连接设备
	if (modbus_connect(m_modBus) != 0)
	{
		HandleError("连接设备失败");
		return false;
	}

	//设置超时时间
	struct timeval t;
	t.tv_sec = 0;
	t.tv_usec = 5000000;   //设置modbus超时时间为5000毫秒 
	if (modbus_set_response_timeout(m_modBus, reinterpret_cast<int>(&t.tv_sec), reinterpret_cast<int>(&t.tv_usec)) == 0)
	{
		//先断开连接,在处理错误
		DisConnectDevice();
		HandleError("设置超时时间失败");
		return  false;
	}
	return true;
}

void CLibModBus::DisConnectDevice()
{
	if (IsValid())
	{
		//关闭
		modbus_close(m_modBus);

		//不要忘记释放资源,否则后续再次连接会产生错误
		modbus_free(m_modBus);

		m_modBus = nullptr;
	}

}

uint16_t* CLibModBus::ReadRegisters(int serverAddress, int numberOfCount)
{
	//初始化数据为-1
	//自己的项目中使用的最大长度为2,所以这里直接这样写
	InitRegistersTab();

	//检查设备是否有效
	if (IsValid())
	{
		//读取寄存器数据
		if (modbus_read_registers(m_modBus, serverAddress, numberOfCount, m_tab_reg) < 0)
		{
			HandleError("读取寄存器数据失败 ");
			//HandleError(std::string(std::string("读取寄存器数据失败,地址为:[%1];读取长度:[%2]  ").arg(serverAddress).arg(numberOfCount)));
		}
		return m_tab_reg;
	}
	return m_tab_reg;
}

uint8_t* CLibModBus::ReadCoils(int serverAddress, int numberOfCount)
{
	InitCoilsTab();
	if (IsValid())
	{
		if (modbus_read_bits(m_modBus, serverAddress, numberOfCount, m_tab_bit) < 0)
		{
			HandleError("读取线圈数据失败 ");
			//HandleError(QString(std::string("读取线圈数据失败,地址为:[%1];读取长度:[%2]  ").arg(serverAddress).arg(numberOfCount)));
		}
		return m_tab_bit;
	}
	return m_tab_bit;
}

bool CLibModBus::ReadCoil(int serverAddress)
{
	InitCoilsTab();
	if (IsValid())
	{
		if (modbus_read_bits(m_modBus, serverAddress, 1, m_tab_bit) < 0)
		{
			HandleError("读取线圈数据失败 ");
			//HandleError(QString(std::string("读取线圈数据失败,地址为:[%1];读取长度:[%2]  ").arg(serverAddress)));
		}
		auto&& c = m_tab_bit[0];
		return m_tab_bit[0];
	}
	return false;
}

void CLibModBus::WriteRegisters(int serverAddress, int numberOfCount, uint16_t* data)
{
	if (modbus_write_registers(m_modBus, serverAddress, numberOfCount, data) < 0)
	{
		HandleError("写入寄存器失败 ");
		//HandleError(QString(std::string("写入寄存器失败,地址为:[%1];写入长度:[%2]  ").arg(serverAddress).arg(numberOfCount)));
	}
}

void CLibModBus::WriteCoil(int serverAddress, bool status)
{
	if (modbus_write_bit(m_modBus, serverAddress, status) < 0)
	{
		HandleError("写入寄存器失败 ");
		//HandleError(QString(std::string("写入寄存器失败,地址为:[%1];写入长度:[%2]  ").arg(serverAddress)));
	}
}

void CLibModBus::InitRegistersTab()
{
	for (unsigned short& i : m_tab_reg)
	{
		i = -1;
	}
}

void CLibModBus::InitCoilsTab()
{
	for (unsigned char& i : m_tab_bit)
	{
		i = -1;
	}
}