#pragma once
#include <string>
#include "include/modbus.h"
class CLibModBus
{
public:
	CLibModBus();
	~CLibModBus();
	void HandleError(const std::string& text = "");

	std::string ErrString()const { return m_errorString; };

	bool IsValid() const { return m_modBus != nullptr; }

	/// <summary>
	/// 连接设备
	/// </summary>
	/// <param name="portName">串口名</param>
	/// <param name="slave">主站地址,我自己的项目中使用的是1,所以增加了默认参数</param>
	/// <returns></returns>
	bool ConnectDevice(const std::string& portName, int slave = 1);

	/// <summary>
	/// 断开设备连接
	/// </summary>
	void DisConnectDevice();

	/// <summary>
	/// 读取寄存器数据
	/// </summary>
	/// <param name="serverAddress">从站地址</param>
	/// <param name="numberOfCount">读取几个</param>
	/// <returns>读取到的数据</returns>
	uint16_t* ReadRegisters(int serverAddress, int numberOfCount);

	/// <summary>
	/// 读取线圈数据
	/// </summary>
	/// <param name="serverAddress">从站地址</param>
	/// <param name="numberOfCount">读取几个</param>
	/// <returns>读取到的数据</returns>
	uint8_t* ReadCoils(int serverAddress, int numberOfCount);

	/// <summary>
	/// 读取指定地址的线圈数据
	/// </summary>
	/// <param name="serverAddress">从站地址</param>
	/// <returns>true/false</returns>
	bool ReadCoil(int serverAddress);

	/// <summary>
	/// 向寄存器写入数据
	/// </summary>
	/// <param name="serverAddress"></param>
	/// <param name="numberOfCount"></param>
	/// <param name="data"></param>
	void WriteRegisters(int serverAddress, int numberOfCount, uint16_t* data);

	/// <summary>
	/// 向线圈中写入数据
	/// </summary>
	/// <param name="serverAddress"></param>
	/// <param name="status"></param>
	void WriteCoil(int serverAddress, bool status);
//signals:
	//void SignalErrorOccurred(std::string errorString);

private:
	void InitRegistersTab();
	void InitCoilsTab();
private:
	modbus_t* m_modBus;
	std::string m_errorString;
	uint16_t m_tab_reg[10] = { 0 };
	uint8_t m_tab_bit[10] = { 0 };
};

